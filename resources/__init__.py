import user_notification_email
import mail

from .user_notifications import (Activate, ApiFailed, CubeBelowMin,
                                 CubeOpened, ExitSurvey, Healthcheck, NoAllocations, NoConnection, 
                                 PaymentDue, PaymentReceived, Recover, NotifyException)

from .support import SupportEmail