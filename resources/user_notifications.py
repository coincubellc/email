from flask_apispec import MethodResource, doc, marshal_with, use_kwargs as use_kwargs_doc
from flask_restful import abort
from webargs import fields, validate
from webargs.flaskparser import use_kwargs 
from user_notification_email import send_notification, send_exception
from datetime import datetime


base_args = {
    'subject': fields.Str(required=True, description='Email subject'),
    'user_id': fields.Int(required=True, description='User ID'),
    'username': fields.Str(required=True, description='Username'),
    'email': fields.Email(required=True, description='User email address'),
    'images': fields.Str(required=False, description='Images dictionary (name, path)')
}


class Activate(MethodResource):
    post_args = {**base_args, **{
        'confirm_url': fields.Str(required=True, description="Account activation URL")
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], description='Register new account')
    def post(self, subject, user_id, username, email, confirm_url, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {'confirm_url': confirm_url}
            return send_notification('activate', subject, user, data, images)
        except Exception as e:
            send_exception('activate', e)
            abort(400)


class ApiFailed(MethodResource):
    post_args = {**base_args, **{
        'exchange_name': fields.Str(required=True, description='Exchange where API has failed'),
        'api_response': fields.Str(required=False, description='API response')
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], description='Alert user of API key failure')
    def post(self, subject, user_id, username, email, 
             exchange_name, api_response, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {'exchange_name': exchange_name, 'api_response': api_response}
            return send_notification('api_failed', subject, user, data, images)
        except Exception as e:
            send_exception('api_failed', e)
            abort(400)


class CubeBelowMin(MethodResource):
    post_args = {**base_args, **{
        'val_fiat': fields.Float(required=True, description='Total fiat value for cube'),
        'usd_min': fields.Float(required=True, description='Minimum maintenance usd')
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Alert user if Cube is below maintanance minimum')
    def post(self, subject, user_id, username, email, 
             exchange_name, btc_val, btc_min, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {
                'val_fiat': val_fiat,
                'usd_min' : usd_min
                }
            return send_notification('cube_below_minimum', subject, data, images)
        except Exception as e:
            send_exception('cube_below_minimum', e)
            abort(400)


class CubeOpened(MethodResource):
    post_args = {**base_args}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], description='Welcome email for new Cube')
    def post(self, subject, user_id, username, email, 
             exchange_name, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            return send_notification('cube_opened', subject, user, data, images)
        except Exception as e:
            send_exception('cube_opened', e)
            abort(400)


class ExitSurvey(MethodResource):
    post_args = {**base_args, **{
        'closed_at': fields.Int(required=True, description='Date when account was closed'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Exit survey email for closed accounts')
    def post(self, subject, user_id, username, email, closed_at, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {'closed_at': datetime.fromtimestamp(closed_at)}
            return send_notification('exit_survey', subject, user, data, images)
        except Exception as e:
            send_exception('exit_survey', e)
            abort(400)


class Healthcheck(MethodResource):
    @doc(tags=['Healthcheck'], description='Endpoint for checking API health')
    def get(self):
        return {'message': 'email API is running'}


class NoAllocations(MethodResource):
    post_args = {**base_args}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Reminder to setup portfolio for new cube')
    def post(self, subject, user_id, username, email, closed_at, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {}
            return send_notification('no_allocations', subject, user, data, images)
        except Exception as e:
            send_exception('no_allocations', e)
            abort(400)


class NoConnection(MethodResource):
    post_args = {**base_args}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Reminder to setup api connection for new cube')
    def post(self, subject, user_id, username, email, closed_at, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {}
            return send_notification('no_connection', subject, user, data, images)
        except Exception as e:
            send_exception('no_connection', e)
            abort(400)


class PaymentReceived(MethodResource):
    post_args = {**base_args, **{
        'paid_at': fields.Int(required=True, description='Paid Until Date as timestamp'),
        'amount_paid': fields.Str(required=True, description='Amount paid'),
        'product': fields.Str(required=True, description='Product name'),
        'months': fields.Int(required=True, description='Months paid for'),
        'paid_until': fields.Int(required=True, description='Paid Until Date as timestamp'),
        'method': fields.Str(required=True, description='Payment method: Bitcoin, Credit Card, etc...'),
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Notify user we have received their payment')
    def post(self, subject, user_id, username, email, 
             paid_at, amount_paid, product, months, paid_until, method, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {
                'paid_at': datetime.fromtimestamp(paid_at), 
                'amount_paid': amount_paid,
                'product': product,
                'months': months,
                'paid_until': datetime.fromtimestamp(paid_until),
                'method': method
                }
            return send_notification('payment_received', subject, user, data, images)
        except Exception as e:
            send_exception('payment_received', e)
            abort(400)


class PaymentDue(MethodResource):
    post_args = {**base_args, **{
        'paid_until': fields.Int(required=True, description='Paid until'),
        'monthly_rate': fields.Decimal(required=True, description='Product monthly rate'),
        'months': fields.Int(required=True, description='Months paid')

    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], description='Payment due email')

    def post(self, subject, user_id, username, email, 
             paid_until, monthly_rate, months, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {
                'paid_until': datetime.fromtimestamp(paid_until), 
                'monthly_rate': monthly_rate, 
                'months': months
                }
            return send_notification('payment_due', subject, user, data, images)
        except Exception as e:
            send_exception('payment_due', e)
            abort(400)


class Recover(MethodResource):
    post_args = {**base_args, **{
        'recover_url': fields.Str(required=True, description='Password reset url')
    }}
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Recover account and change password email')

    def post(self, subject, user_id, username, email, 
             recover_url, images=None):
        try:
            user = {'id': user_id, 'username': username, 'email': email}
            data = {'recover_url': recover_url}
            return send_notification('recover', subject, user, data, images)
        except Exception as e:
            send_exception('recover', e)
            abort(400)

class NotifyException(MethodResource):
    post_args = {
        'type': fields.Str(required=True, description='function where exception occured'),
        'error': fields.Str(required=True, description='error message')
    }
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['User Notifications'], 
         description='Exception notification for dev')

    def post(self, type, error):
        data = {'type': recover_url, 'error': error}
        return send_exception(type, error)

