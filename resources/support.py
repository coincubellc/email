from flask_apispec import MethodResource, doc, marshal_with, use_kwargs as use_kwargs_doc
from flask_restful import abort
from webargs import fields, validate
from webargs.flaskparser import use_kwargs 
from mail import send_mail


post_args = {
    'subject': fields.Str(required=True, description='Email subject'),
    'username': fields.Str(required=True, description='Username'),
    'email': fields.Email(required=True, description='User email address'),
    'account_type': fields.Str(required=True, description='Free, Basic, Pro'),
    'message': fields.Str(required=True, description='User support message')
}

class SupportEmail(MethodResource):
    @use_kwargs(post_args, locations=('json', 'form'))
    @use_kwargs_doc(post_args, locations=('json', 'form'))
    @doc(tags=['Support'], description='Send support email')
    def post(self, subject, username, email, account_type, message):
        support_email = 'support@coincube.io'
        full_subject = f'{account_type} account support request \
                            from {username} at {email}: {subject}'
        try:
            return send_mail(
                    support_email, 
                    support_email,
                    full_subject,
                    message)
        except:
            abort(500)