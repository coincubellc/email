import os
from flask import Flask
from flask_restful import Resource, Api
from flask_apispec import FlaskApiSpec
from resources import *


app = Flask(__name__)
api = Api(app)
docs = FlaskApiSpec(app)

# Configurations
app.config.from_pyfile('config.py')

resources = {
    # User Notification Email Endpoints
    '/user_notification/activate': Activate,
    '/user_notification/api_failed': ApiFailed,
    '/user_notification/cube_below_min': CubeBelowMin,
    '/user_notification/cube_opened': CubeOpened,
    '/user_notification/exit_survey': ExitSurvey,
    '/user_notification/no_allocations': NoAllocations,
    '/user_notification/no_connection': NoConnection,
    '/user_notification/payment_due': PaymentDue,
    '/user_notification/payment_received': PaymentReceived,
    '/user_notification/recover': Recover,
    '/user_notification/notification_exception': NotifyException,
    # Support Request Endpoint
    '/support_request': SupportEmail,
    # Healthcheck Endpoint
    '/health': Healthcheck,
}

for key, value in resources.items():
    # Add API resources
    api.add_resource(value, key)
    # Register documentation
    docs.register(value)


if __name__ == '__main__':
    print ("Starting server..")
    #PORT = int(os.getenv('PORT'))
    #HOST = os.getenv('HOST')
    #DEBUG = app.config['DEBUG']
    #app.run(debug=DEBUG, host=HOST, port=PORT)
