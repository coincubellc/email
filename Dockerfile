FROM continuumio/anaconda3:5.0.1
RUN apt-get update && apt-get install -y build-essential

ADD requirements.txt /email/requirements.txt
RUN pip install -r /email/requirements.txt

ADD . /email
WORKDIR /email

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

EXPOSE 9090

ENTRYPOINT ["docker-entrypoint.sh"]