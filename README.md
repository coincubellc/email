<h2>email</h2>
This repo contains the Coincube Email API.

<h3>Getting Started</h3>
In order to get up and running with this repo:

<h3>Docker Setup</h3>
You will need <a href="https://docker.com" target="_blank">Docker</a>.

Build the Docker container:
`docker-compose build`<br>

Run the Docker container:
`docker-compose up`<br>

<h3>API Documentation</h3>
Once the application is running, you can view Swagger documentation at:
<a href="http://0.0.0.0:9090/swagger-ui">http://0.0.0.0:443/swagger-ui</a><br>
JSON is available at: <a href="http://0.0.0.0:9090/swagger">http://0.0.0.0:443/swagger</a>

<h3>Launch the App</h3>
Once Docker is running, the api endpoints will be available at `https://0.0.0.0:9090`<br>

<h6>Working on the App</h6>
Any time you save a `.py` file with changes, the Flask application will reload.<br>

<h3>Troubleshooting</h3>
Make sure that old docker networks are removed<br>
'docker-compose down' should remove them<br>
Check with 'docker network ls'<br>
If you see errors related to Disk Space, you may need to reset Docker:<br>
To do so: Docker -> Preferences -> Reset -> Remove All Data