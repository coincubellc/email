import smtplib
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from flask import current_app


def send_mail(from_field, to_field, subject, message, images = None):
    try:
        msg = MIMEMultipart('alternative')
        msg.attach(MIMEText(message, 'html'))
        msg['From'] = from_field
        msg['Subject'] = subject
        msg['To'] = to_field

        # if images:
        #     for name, path in images.items():
        #         fp = open(path, 'rb')
        #         msgImage = MIMEImage(fp.read())
        #         fp.close()

        #         # Define the image's ID as referenced above
        #         msgImage.add_header('Content-ID', '<' + name + '>')
        #         msg.attach(msgImage)

        s = smtplib.SMTP('smtp.mandrillapp.com', 587)
        s.login(current_app.config['MANDRILL_USERNAME'], current_app.config['MANDRILL_PASSWORD'])
        s.send_message(msg)
        s.quit()


    except:
        print(str(sys.exc_info()))
        return False

    return True



