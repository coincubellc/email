#!/bin/bash

uwsgi --http 0.0.0.0:${PORT} --wsgi-file app.py --callable app --processes 4 --threads 2