from mail import send_mail
from flask import current_app, render_template


def send_notification(template, subject, user = {}, data = {}, images = None, echo = False):
    user_id = user['id']
    username = user['username']
    email = user['email']
    if echo:
        print("Notify " + template.replace("_", " ") + ", user_id: " + str(user_id) + ", email: " + email)

    if not data:
        data = {}
    data['app_title'] = current_app.config['APP_TITLE']
    data['admin_email'] = current_app.config['ADMIN_EMAIL']
    data['support_email'] = current_app.config['SUPPORT_EMAIL']
    data['site_url'] = current_app.config['BASE_URL']
    data['img_url'] = data['site_url'] + "/static/img"
    data['name'] = username

    if not hasattr(data, "unsubscribe"):
        data['unsubscribe'] = False

    if echo:
        print(data)

    html = render_template('user_notification_email/' + template + '.html', data = data)

    sent = False
    if not current_app.config['DEBUG']: 
        # send to user
        if send_mail(current_app.config['ADMIN_EMAIL'], email, subject, html, images):
            sent = True
            
    # Send to admin and dev
    subject = subject + " - user id: " + str(user_id) + " - " + email
    to = current_app.config['ADMIN_EMAIL'] + ", " + current_app.config['OPS_EMAIL']

    send_mail(current_app.config['ADMIN_EMAIL'], to, subject, html, images)

    if sent:
        return {'message': 'Email sent.'}
    else:
        return {'message': 'Debug mode. Email not sent.'}

def send_exception(notification, error):

    text = 'Caught exception: ' +  str(error)
    subject = "Notify User Exception in " + notification
    sender = current_app.config['ADMIN_EMAIL']
    to = current_app.config['ADMIN_EMAIL'] + ", " + current_app.config['OPS_EMAIL']

    if(send_mail(sender, to, subject, text)):
        return {'message': 'Exception email sent.'}




